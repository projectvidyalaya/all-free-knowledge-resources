Simulations :
    - https://phet.colorado.edu/
    
- https://www.bighistoryproject.com/home

- www.gutenberg.org/robot/harvest (all boks in guntenberg project)

- https://librivox.org/ (Community AudioBook Project)

- http://www.gutenberg.org/ (Open Books)

- https://www.youtube.com/channel/UCR_HiQVSl8_KZ9ItuleTnpQ/videos (MIT Blossoms)

- https://www.youtube.com/user/MIT/playlists (MIT OpencourseWare)
- https://ocw.mit.edu/index.htm (MIT OpencourseWare)

- https://www.youtube.com/channel/UC7GbVKqHPXww1acL1x9DNQw/playlists (Tmailnadu classes online)

- http://wiki.kiwix.org/wiki/Content (Kiwix all downloads)

- http://www.publicbooks.org
- https://www.youtube.com/channel/UCYO_jab_esuFRV4b17AJtAw (a lot of resources )
- https://www.qdl.qa/en (Quatar Library free to use and reuse, books, videos etc)
- https://soundcloud.com/qatar-digital-library (Quatar library audio version)